import User from '../../server/models/User';

export default {
  Query: {
    hello: (_, { name }) => `Hello ${name || 'World'}`,
    users: () => User.find(),
    age: (_, { age }) => User.find({ age })
  },
  Mutation: {
    createUser: async (_, { user }) => {
      const newUser = await new User({
        name: user.name,
        email: user.email,
        age: user.age
      });

      return new Promise((resolve, reject) => {
        newUser.save((error, response) => {
          error ? reject(error) : resolve(response);
        });
      })
    },
    updateUser: (_, { _id, user }) => User.findByIdAndUpdate(_id, { $set: { ...user } }, { new: true }),
    deleteUser: (_, { _id }) => User.findByIdAndDelete(_id)
  }
}
