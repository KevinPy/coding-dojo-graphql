const { GraphQLServer } = require('graphql-yoga');
import schema from './api';
import { startDBNoLogin } from './server/connection';
import { models } from './server/models';

const db = startDBNoLogin({
  user: '',
  password: '',
  db: 'codingdojo-graphql',
  url: '127.0.0.1:27017'
});

const context = {
  models,
  db
}

const options = {
  port: process.env.PORT || 4000,
  endpoint: "/graphql",
  subscriptions: "/subscriptions",
  playground: "/playground"
};


const server = new GraphQLServer({ schema, context });

server.start(options, ({ port }) => console.log(`Server is running on http://localhost:${port}`));
