import mongoose from 'mongoose';

// Hack for mongo promise
mongoose.Promise = global.Promise;

// With user / password
export const startDB = ({ user, password, url, db }) => mongoose.connect(`mongodb://${user}:${password}@${url}/${db}`);

// Without user / password
export const startDBNoLogin = ({ url, db }) => mongoose.connect(`mongodb://${url}/${db}`);
