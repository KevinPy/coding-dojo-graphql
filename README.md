# Coding Dojo GraphQL

Coding Dojo Live: https://www.youtube.com/watch?v=YNVpzBAc5Dw

## Install
### MongoDB
Lien: https://docs.mongodb.com/manual/administration/install-community/
Vous devez avoir MongoDB installé en local sur votre ordinateur.
Une fois installé, démarrez MongoDB, et c'est tout.

### Robo 3T
Lien: https://robomongo.org/
Ce programme vous permet de visualiser votre infrastructure MongoDB. Vous pouvez vous connecter en localhost sur le port 27017, sans user ni mot de passe.

### Projet
```
npm i
npm run start
```
*Attention: MongoDB doit être lancé pour que le projet se lance.*
